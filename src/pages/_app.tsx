// Importação das bibliotecas necessárias
import { type AppType } from "next/app";
import { type Session } from "next-auth";
import { SessionProvider } from "next-auth/react";
import "@fortawesome/fontawesome-svg-core/styles.css";

import { api } from "~/utils/api";

// Importação do arquivo de estilo global
import "~/styles/globals.css";

// Definição do componente MyApp como uma classe tipada AppType
const MyApp: AppType<{ session: Session | null }> = ({
  Component,
  pageProps: { session, ...pageProps },
}) => {
  // O componente recebe como prop a sessão do usuário, que é passada para o SessionProvider
  return (
    <SessionProvider session={session}>
      <Component {...pageProps} />
    </SessionProvider>
  );
};

// O componente é exportado envolvido na função withTRPC, que adiciona funcionalidades de RPC
// e facilita a comunicação com a API
export default api.withTRPC(MyApp);
