import { type NextPage } from "next";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import CharacterSilhouette from "~/components/Avatar";
import DonationTable from "~/components/DonationTable";
import Footer from "~/components/Footer";
import MovingBackground from "~/components/MovingBackground";
import Navbar from "~/components/Navbar";

const Home: NextPage = () => {
  const [client, setClient] = useState(false);

  useEffect(() => {
    setClient(true);
  }, []);

  return (
    <>
      <Head>
        <title>HypeMatrix</title>
        <meta name="description" content="Um projeto de procrastinação" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="relative z-10 flex h-screen flex-col items-center justify-center p-10">
        <Navbar />
        {client && <MovingBackground />}

        <div className="my-20 flex w-full items-center justify-center space-x-8">
          <div id="screen-data" className="w-2/5">
            <h1 className="pb-3 text-white">Stats</h1>
            <div className="red-border mb-3" />
            <div className="grid grid-cols-2 gap-2">
              <div className="p-2">
                <h2 className="text-white">País</h2>
                <p>Datahaven</p>
              </div>
              <div className="p-2">
                <h2 className="text-white">Cidade</h2>
                <p>Hiveshire</p>
              </div>
              <div className="p-2">
                <h2 className="text-white">Colaboradores</h2>
                <p>0</p>
              </div>
              <div className="p-2">
                <h2 className="text-white">Votos Registrados</h2>
                <p>0</p>
              </div>
            </div>
          </div>
          <div className="w-1/5">
            <CharacterSilhouette />
          </div>
          <div id="screen-data" className="w-2/5">
            <h1 className="pb-3 text-white">Votação aberta</h1>
            <div className="red-border mb-3" />
            <div className="p-2">
              <p>- Fulano votou pelo nome "Jurunga" </p>
              <p>- Ciclano votou pelo sexo "Feminino" </p>
              <p>- Beltrano votou pela atitude "Pessimista" </p>
              <p>- Silvano votou pelo estilo "Shoegaze" </p>
            </div>
          </div>
        </div>

        <DonationTable />
        <Footer />
      </main>
    </>
  );
};

export default Home;
