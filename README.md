# Projeto de criação de artista fictício

Neste projeto revolucionário, convidamos você a colaborar na criação de um personagem fictício - um artista - cujo sucesso será impulsionado por decisões coletivas e financiamento colaborativo. O objetivo é desafiar as normas da indústria artística, demonstrando que o sucesso pode ser alcançado independentemente da realidade física do artista.

Ao unir forças, questionaremos os limites entre o real e o fictício. Os participantes moldarão a identidade, estilo e história do artista através de votações. Este experimento social busca transformar a indústria artística, abrindo caminho para novas formas de expressão e reconhecimento.

Apoiadores do projeto, através de doações e engajamento, ajudarão a promover esse artista fictício, levando-o ao sucesso e tornando-o uma figura reconhecida na mídia.

Participe desta jornada inovadora e criativa, buscando mudar a indústria artística e reinventar a forma como a arte é percebida e valorizada. Seja um agente dessa transformação e ajude a criar o artista mais disruptivo e inovador do nosso tempo.

## Como executar o projeto

1. Clone o repositório
2. Instale as dependências: `npm install`
3. Inicie o servidor de desenvolvimento: `npm run dev`
4. Acesse o projeto em seu navegador em: `http://localhost:3000`